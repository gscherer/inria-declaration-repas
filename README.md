This document is a template for a "déclaration sur l'honneur" to get
meal expenses reimbursed during a "mission" (work trip) at INRIA. It
might be useful to members of other French research institutions.

The current reimbursement rules (2019-?) are the following:

- If your costs, minus meals and housing (so: travel and
  conference registration), are below 30€, you do not need to provide
  any justificative to get meal expenses reimbursed -- at a fixed
  price (forfait).

- If you declare transportation and registration costs above 30€, you
  have to provide justificatives for meal expenses to get meals
  reimbursed, but INRIA accept a self-declaration (déclaration sur
  l'honneur) as a justificative.

  (I believe that, whether you provide precise justificatives or
  a déclaration, you get reimbursed at a fixed price (forfait) instead
  of according to your precise expenses.)

The present document is a template to write these "déclaration sur
l'honneur".

Note: INRIA typically expects breakfast to be included with the
housing, and I am not sure that they would consider breakfast as
a reimbursable expense. I only declare at most two meals per day, for
lunch (when not provided by the event) and dinner.
